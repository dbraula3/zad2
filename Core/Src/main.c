/**
  ******************************************************************************
  * @file    main.c
  * @author  Danie
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/

#include "stm32f3xx.h"

volatile int8_t adc_data_flag = 0;
uint8_t place_cnt=31;
volatile int32_t filter_buffer[32];
uint8_t N=1;
int8_t T=10;
uint8_t counter = 0;
uint16_t temp;

uint16_t V25 = 1430; // [miliVolts]
uint16_t Avg_slope = 4300; //   [microVolts/degree]
uint16_t SupplyVoltage = 3300; // [miliVolts]
uint16_t ADCResolution = 4095.0;
uint32_t Vsense;

// RX
volatile int8_t rx_buf[4] = { 'a', 'a', 'a', 'a'};
// UART TX
volatile int8_t tx_buf[] = "00.00 stopniC\r\n";
volatile int32_t tx_buf_size = 0;
volatile int32_t tx_transmitted = 0;
uint8_t send_flag=0;

void ADC1_2_IRQHandler(void) {
	if (0 != (ADC_ISR_EOC & ADC1->ISR)) {
		if(counter == T){
			counter = 0;
			send_flag=1;
		}
		adc_data_flag = 1;
		place_cnt+=1;
		if (place_cnt == 32)
			place_cnt=0;
		filter_buffer[place_cnt] = ADC1->DR;
		counter+=1;
	}
}

void reset_buffer(){
	for(uint8_t i=0; i<4; i++)
		rx_buf[i]='a';
}

int32_t response_filter(){
	uint32_t filtered_data = 0;
	uint32_t filter_temp_sum = 0;
	uint8_t place_temp=0;
	place_temp=place_cnt;
	// count average
	for( uint8_t i = 0; i < N; i++){
		filter_temp_sum+=filter_buffer[place_temp];
		if(place_temp==0)
			place_temp+=32;
		place_temp-=1;
	}
	Vsense = (SupplyVoltage*filter_temp_sum)/ADCResolution;// Przeliczenie wartosci zmierzonej na napiecie
	filtered_data = ((V25*N-Vsense)*1000*1000/Avg_slope)+25000*N;// Obliczenie temperatury
	filtered_data=filtered_data/N;
	return filtered_data;
}

uint8_t finish_ready=0;
void USART2_IRQHandler(void) {
	uint8_t temp = USART_ISR_TXE & USART2->ISR;
	if (0 != (USART_ISR_TXE & USART2->ISR)) {
		if ((0 != tx_buf_size) && (tx_transmitted < tx_buf_size)) {
			USART2->TDR = (uint32_t) tx_buf[tx_transmitted];
			tx_transmitted += 1;
		}
		if (tx_transmitted == tx_buf_size) {
			USART2->CR1 &= ~USART_CR1_TXEIE;
		}
	}


	temp = USART_ISR_RXNE & USART2->ISR;
	if (0 != (USART_ISR_RXNE & USART2->ISR))
	{
		temp = USART2->RDR;
		if (temp=='T' || temp=='N'){
			reset_buffer();
			finish_ready=0;
			rx_buf[0]=temp;
		}
		else if ((rx_buf[0]=='T' || rx_buf[0]=='N') && (temp-'0'>= 0)  &&  (temp-'0'<= 9))
		{
			if (rx_buf[1]=='a')
			{
				rx_buf[1]=temp;
				finish_ready=1;
			}
			else if (rx_buf[2]=='a')
			{
				rx_buf[2]=temp;
				finish_ready=1;
			}
			else if (rx_buf[3]=='a')
			{
				rx_buf[3]=temp;
				finish_ready=1;
			}
			else{
				reset_buffer();
				finish_ready=0;
			}
		}
		else if((temp==' ') && finish_ready==1)
		{
			if (rx_buf[2]=='a')
				temp = rx_buf[1]-'0';
			else if (rx_buf[3]=='a')
				temp = 10*(rx_buf[1]-'0') + (rx_buf[2]-'0');
			else
				temp = 100*(rx_buf[1]-'0') + 10*(rx_buf[2]-'0') + rx_buf[3]-'0';
			if (rx_buf[0]=='T'){
				if(temp>=0 && temp<=100){
					T=temp;
					strcpy(tx_buf, "set new T \r\n");
					tx_transmitted = 0;
					USART2->CR1 |= USART_CR1_TXEIE;
					counter=0;
				}
			}
			else{
				if(temp<=32 && temp>0){
					N=temp;
					strcpy(tx_buf, "set new N \r\n");
					tx_transmitted = 0;
					USART2->CR1 |= USART_CR1_TXEIE;
				}
			}
			reset_buffer();
			finish_ready=0;
		}
		else if(temp==' '){}
		else{
			reset_buffer();
			finish_ready=0;
		}
	}
}


int main(void)
{
	// ------------- RCC -------------
	RCC->CR |= RCC_CR_HSION; // enable HSI
	while (0 == (RCC->CR  & RCC_CR_HSIRDY)) { // wait until RDY bit is set
		// empty
	}
	RCC->CFGR |= RCC_CFGR_SW_0; // set SYSCLK to HSI8

	RCC->AHBENR |= RCC_AHBENR_GPIOAEN; // enable GPIOA clock
	RCC->APB1ENR |= RCC_APB1ENR_USART2EN | RCC_APB1ENR_TIM6EN; // enable USART2 and TIM6 clocks
	RCC->AHBENR |= RCC_AHBENR_ADC12EN; // enable ADC clock

	// ------------- ADC1 -------------
	ADC1->IER |= ADC_IER_EOCIE; // end of conversion interrupt enable
	ADC1->CR &= ~(ADC_CR_ADEN); // disable ADC
	ADC1->CR &= ~(ADC_CR_ADVREGEN);  //transition state
	ADC1->CR |= ADC_CR_ADVREGEN_0; // enable voltage regulator

	ADC1->CFGR |= ADC_CFGR_EXTEN_0; // enable hardware trigger, select rising edge
	ADC1->CFGR |= (ADC_CFGR_EXTSEL_0 | ADC_CFGR_EXTSEL_2 | ADC_CFGR_EXTSEL_3);
	ADC12_COMMON->CCR |= ADC_CCR_CKMODE_0; // clock mode - synchronous clock (PCLK) divided by 1
	ADC1->SQR1 |= (16 << ADC_SQR1_SQ1_Pos); // choose 16 canal for first conversion
	ADC1->SMPR2 |= ( 7 << ADC_SMPR2_SMP16_Pos); // sampling time for canal 16,  601.5 ADC clock cycles
	ADC1->CFGR &= ~ADC_CFGR_RES;  //max 12 bites resolution
	ADC12_COMMON->CCR |= ADC_CCR_TSEN; // temperature sensor enable

	ADC1->CR &= ~(ADC_CR_ADCALDIF);
	ADC1->CR |= ADC_CR_ADCAL; // start calibration
	while (0 != (ADC_CR_ADCAL & ADC1->CR)) { // wait until calibration is completed
		// empty
	}

	// Steps to enable ADC
	ADC1->ISR |= ADC_ISR_ADRDY; // clear ADREADY bit
	ADC1->CR |= ADC_CR_ADEN; // enable ADC
	while (0 == (ADC_ISR_ADRDY & ADC1->ISR)) { // wait until ADC is ready
		// empty
	}
	ADC1->CR |= ADC_CR_ADSTART; // start operation (wait for trigger)

	// ------------- TIM6 -------------
	TIM6->CR2 |= TIM_CR2_MMS_1; // set update event as TRGO signal source
	TIM6->PSC = 8000 - 1;
	TIM6->ARR = 100 - 1; // set period to 100ms

	// ------------- USART2 -------------
	USART2->BRR = 70 - 1; // 8 000 000 / 9600 = 833.33
	USART2->CR1 =  USART_CR1_RE | USART_CR1_TE | USART_CR1_UE;

	// ------------- GPIOA (pin 2 - TX & 3 - RX) -------------
	GPIOA->AFR[0] &= ~0x0000FF00;
	GPIOA->AFR[0] |=  0x00007700;
	GPIOA->MODER &= ~(GPIO_MODER_MODER3_1 | GPIO_MODER_MODER3_0 | GPIO_MODER_MODER2_1 | GPIO_MODER_MODER2_0);
	GPIOA->MODER |= (GPIO_MODER_MODER2_1 | GPIO_MODER_MODER3_1);
	GPIOA->PUPDR |= GPIO_PUPDR_PUPDR3 | GPIO_PUPDR_PUPDR2;
	GPIOA->OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR3_1 | GPIO_OSPEEDER_OSPEEDR3_0 | GPIO_OSPEEDER_OSPEEDR2_1 | GPIO_OSPEEDER_OSPEEDR2_0);

	// ------------- NVIC -------------
	NVIC_EnableIRQ(ADC1_2_IRQn); // nvic interrupt enable (ADC1 interrupt)
	NVIC_EnableIRQ(USART2_IRQn); // nvic interrupt enable (USART2 interrupt)
	(void)USART2->RDR;
	USART2->CR1 |= USART_CR1_RXNEIE;

	// TIM6 enable
	TIM6->CR1 |= TIM_CR1_CEN; // count enable

	while (1) {
		if ((0 != adc_data_flag) && (tx_buf_size == tx_transmitted)) {
			adc_data_flag = 0;

			// Calculate temperature
			int32_t filtered_data = response_filter();

			// Transmit data
			if (send_flag == 1 && T!=0){
				send_flag = 0;
				strcpy(tx_buf, "00.00 stopniC\r\n");
				tx_buf[0] = '0' + (int)(filtered_data / 10000);
				tx_buf[1] = '0' + (int)((filtered_data%10000) / 1000);
				tx_buf[3] = '0' + (int)((filtered_data%1000) / 100);
				tx_buf[4] = '0' + (int)((filtered_data%100) / 10);
				tx_buf_size = strlen((const char *) tx_buf);
				tx_transmitted = 0;
				USART2->CR1 |= USART_CR1_TXEIE;
			}
			if (T == 0){
				strcpy(tx_buf, "00.00 stopniC\r\n");
				tx_buf[0] = '0' + (int)(filtered_data / 10000);
				tx_buf[1] = '0' + (int)((filtered_data%10000) / 1000);
				tx_buf[3] = '0' + (int)((filtered_data%1000) / 100);
				tx_buf[4] = '0' + (int)((filtered_data%100) / 10);
				tx_buf_size = strlen((const char *) tx_buf);
				tx_transmitted = 0;
				USART2->CR1 |= USART_CR1_TXEIE;
				T=-1;
			}
		}
	}
}
